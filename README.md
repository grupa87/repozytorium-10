# Powiększony nagłówek 

Prażone *jabłka*. Prażone *jabłka* do słoików na **zimę**, to wspaniały i bardzo uniwersalny przepis. Sprawdzą się nie tylko do szarlotek, ale i do placków lub naleśników, a nawet na kanapki lub do ryżu. - prosty *przepis*. - tylko kilka składników. - bez **pasteryzowania** słoików. Czytaj dalej: Prażone jabłka. 4 słoiki po 500 ml 30 minut. [zrodlo](https://aniagotuje.pl/tag/jablka) 

![zdjecie przestawiajace jablko](Red_Apple.jpg)
[zrodlo zdjecia](https://pl.wikipedia.org/wiki/Jab%C5%82ko#/media/Plik:Red_Apple.jpg)


Sezon na jabłka. Ciasta i desery z jabłkami, babeczki, ciastka, tarty jabłkowe. KATEGORIE. SZUKAJ. ZALOGUJ × Wybierz jedną z metod logowania: ... PRZEPISY NA MAILA. Szukaj przepisu. Szukaj w opisach. Szukaj wg kryteriów. Szukaj wg tagów. Na Okazje. [zrodlo](https://domowe-wypieki.pl/jablka)


Prażone jabłka. Prażone jabłka do słoików na zimę, to wspaniały i bardzo uniwersalny przepis. Sprawdzą się nie tylko do szarlotek, ale i do placków lub naleśników, a nawet na kanapki lub do ryżu. - prosty przepis. - tylko kilka składników. - bez pasteryzowania słoików. *Czytaj dalej: Prażone jabłka.* 4 słoiki po 500 ml 30 minut. [zrodlo](https://aniagotuje.pl/tag/jablka) 

> Co najmniej jeden cytat.

1. Lista
2. Lista
    1. Lista
    2. Lista
3. Lista

- First item
- Second item
- Third item
    - Indented item
    - Indented item
- Fourth item 


```cpp
#include <iostream>

int main() {
  std::cout << " ";
}
```

Kod programu wykona `main()`.

